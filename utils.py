"""
Various utils functions for python scripting, including Blender scripting and
3D vision and math

version 1.0.3

written by J. Glaser
"""

import numpy as np


class ExitOK(Exception):
    pass
class ExitError(Exception):
    pass

def hideRenderCollection(collectionName, visibilityStates, hide):
    """ For a collection name given, hides all child objects from render """
    if not collectionName in bpy.data.collections:
        print('No collection with name [{}] exists. This is not error of script. Check that the wished collection exists'.format(collectionName));
        raise ExitError;

    for obj in bpy.data.collections[collectionName].objects:
        #no need to go recursivelly, as the collection returns all objects recursivelly already
        hideRender(obj, visibilityStates, hide, False);

def revertHideRenderCollection(collectionName, visibilityStates):
    if not collectionName in bpy.data.collections:
        print('No collection with name [{}] exists. This is not error of script. Check that the wished collection exists'.format(collectionName));
        raise ExitError;

    for obj in bpy.data.collections[collectionName].objects:
        #no need to go recursivelly, as the collection returns all objects recursivelly already
        revertHideRender(obj, visibilityStates, False);

#hide / show all children recursivelly from render
def hideRender(ob, visibilityStates, hide, recursivelly = True):
    #save state
    visibilityStates[ob.name] = ob.hide_render;
    ob.hide_render = hide;

    if recursivelly:
        for child in ob.children:
            hideRender(child, visibilityStates, hide, recursivelly);

def revertHideRender(ob, visibilityStates, recursivelly = True):
    ob.hide_render = visibilityStates[ob.name];
    if recursivelly:
        for child in ob.children:
            revertHideRender(child, visibilityStates, recursivelly);

def projectObjectPos(P, obj):
    """
    Projects the object's 3D position to the image coordinates, using the camera projection matrix

    Parameters
    ----------
    P : type
        The camera projection matrix
    obj : type
        The Blender object, which's position is going to be projected

    Returns
    -------
    ndarray
        The euclidean coordinates of the projected point.

    """
    posW_homo = np.array([
    obj.matrix_world.translation.x,
    obj.matrix_world.translation.y,
    obj.matrix_world.translation.z,
    1]);

    #project the origin point using camera
    point_projected = P @ posW_homo.reshape(4, 1);
    #normalize
    x = point_projected[0] / point_projected[2];
    y = point_projected[1] / point_projected[2];
    return np.array([x, y]);

def getRotEulerZ(index, anglesTotal):
    return -1* index * (360/anglesTotal) * (math.pi/180);

def getMatrixCol(M, idx):
    """
    Gives xth column of a matrix

    Parameters
    ----------
    M : ndarray
        The matrix
    idx : TYPE
        the zero based index of the column to get.

    Returns
    -------
    ndarray
        DESCRIPTION.

    """
    return np.array(np.array(M).T[idx]);

def addColToMatrix(M, colNp):
    """


    Parameters
    ----------
    M : ndarray
        DESCRIPTION.
    colNp : ndarray
        DESCRIPTION.

    Returns
    -------
    ndarray
        DESCRIPTION.

    """

    # add column
    return np.hstack( ( M, colNp ) );

def toHomo(pt):
    """
    Converts the point to homogeneous coordinates

    Parameters
    ----------
    pt : list
        the point to convert.

    Returns
    -------
    list
        DESCRIPTION.

    """
    return [pt[0], pt[1], 1];

def runTests():
    r = getMatrixCol(np.array([
        [1, 1, 1],
        [2, 3, 6],
        [50, 50, 50],
        ]), 0);
    assert r[0] == 1;
    assert r[1] == 2;
    assert r[2] == 50;
