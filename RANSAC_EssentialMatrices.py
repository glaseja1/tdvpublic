import numpy as np;
import random;
import math

from tdvTask2Utils import *
from calcEssentialMatrices import *
from decomposeEssentialMatrices import *
from triangulate import *


import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def RANSAC_pickFiveAndFindModelEssMat(inputPts1, inputPts2, corresp, K):
    """
     Will pick 5 random points, and calculate the essential matrices from it, 
     which will serve as a model

    Parameters
    ----------
    inputPts1 : TYPE
        The input points we work with, set 1.
    inputPts2 : TYPE
        The input points we work with, set 2.
    correspoPoll_oneWay : TYPE
        DESCRIPTION.
    K : TYPE
        The calibration matrix.
    Returns
    -------
    The essential matrices.

    """
    # 1. Chose a random 5-tuple of correspondences
    indexes = random.sample(range(0, corresp.shape[1]), 5)
         
    #the five chosen points
    pts_5 = [];
    u1p = [];
    u2p = [];
    for i in range(5):
        aidx = corresp[0, indexes[i]];
        bidx = corresp[1, indexes[i]];
        a = [getPointX(aidx, inputPts1), getPointY(aidx, inputPts1)];
        b = [getPointX(bidx, inputPts2), getPointY(bidx, inputPts2)];
        pts_5.append([a, b]);
     
        u1p.append(a);
        u2p.append(b);

     
    #print(pts)
    # 2. Compute essential matrices E
    # 3. Decompose every E into rotations R and translations t - four combinations
    # create point corresp for the algorithm
    u1p = np.array(u1p).T
    u2p = np.array(u2p).T
    #print('u1p: {}'.format(u1p));
    #print('u2p: {}'.format(u2p));
     
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # 2. Compute essential matrices E
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     
    # returns multiple essential matrices
    Es = calcEssentialMatrices(u1p, u2p, K);
    #Es is an array of Essential matrices
     
    # E – homogeneous 3 × 3 matrix; 9 numbers up to scale
    # vi = K-1 * m
    # v'i = K-1 * m'
    # m, m' are the corrrespondencies
     
    #testing playground
    #testingPlayground.run();
    return Es, pts_5;

def RANSAC_triangulate_findSolWherePointsInFrontOfCam(
        Es, Rs, ts, pts_5, K, DEBUG = False):
    """
    For multiple E, R, T matrices and 5 point correspondencies given,
    Will triangulate for all the point correspondencies, 
    and return such E, R, T, for which all the 3D points lie in front of both cameras

    if there is no solution, then the returned solutionTdPts will be empty
    Parameters
    ----------
    Es : TYPE
        Multiple E matrices.
    Rs : TYPE
        The R matrices, for each Essential matrix.
    ts : TYPE
        the T matrices, for each essential matrix.
    pts_5 : TYPE
        the 5 chosen point samples we work with.

    Returns
    -------
    The essential matrix
    The Rotation matrix 
    T matrix 
    The 3D points that do lie in front of the both cameras. Empty, if there was no solution
    The projection matrix P1. This matrix is NOT multiplied by K
    The projection matrix P2. This matrix is NOT multiplied by K
    """
    # Select such R, t from the four combinations for which the five
    # reconstructed 3D points lie in front of both cameras

    # in order to test that, the cameras necessary for triangulation of
    # u1 and u2 must be constructed. Since the points have allready K undone,
    # the cameras will be P1 = [I|0] (canonical one) and P2 = [R|t]

    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # construct cameras for triangulation
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    P1 = constructCameraCanonical()
    if(DEBUG):
        print('P1: {}'.format(P1))

    # the Essential matrix for which we found the solution
    solutionE = None;
    # the R matrix for which we found the solution
    solutionR = None;
    solutionT = None;
    # the projected 3D points 
    solutionTdPts = [];
    solutionP2 = None;

    #put the five chosen points to vector
    u1p = [];
    u2p = [];
    for i in range(5):
        u1p.append(pts_5[i][0]);
        u2p.append(pts_5[i][1]);
    u1p = np.array(u1p).T
    u2p = np.array(u2p).T
    
    #for each essential matrix 
    for esIdx in range(len(Rs)):
        if(DEBUG):
            print('For essential matrix #{}'.format(esIdx));
        
        #iterate all calculated R,t pair matrices we got from above, for the one essential matrix 
        for c in range(len(Rs[esIdx])):
            if(DEBUG):
                print('  For R,t pair #{}'.format(c));
            
            P2 = constructCamera(Rs[esIdx][c], ts[esIdx][c]);
            #print('P2: {}'.format(P2))
            
            # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # triangulate, and 
            # select such R, t where all the 3D points are in front of both cameras
            # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            ptsInFront = [];
            for i in range(len(pts_5)):
                inFront, TDPoint = arePtsInFrontOfCameras(P1, P2, getMatrixCol(u1p, i), getMatrixCol(u2p, i), K);
                if (inFront):
                    ptsInFront.append(TDPoint);
                    
            if(DEBUG):
                print('# of pts in front of cameras: {}'.format(len(ptsInFront)));
            # if all the points were in front of the camera, we got solution
            if(len(ptsInFront) >= 5):
                solutionR = Rs[esIdx][c];
                solutionE = Es[esIdx];
                solutionT = ts[esIdx][c];
                if(DEBUG):
                    print('Got solution');    
                    print('E: {}'.format(solutionE));       
                    print('R: {}'.format(solutionR));  
                solutionTdPts = ptsInFront;    
                solutionP2 = P2;
                break;
            
    return solutionE, solutionR, solutionT, solutionTdPts, P1, solutionP2;

def arePtsInFrontOfCameras(P1, P2, corr1, corr2, K):
    """    
    Given 2 points, which are coresspondent, in image coordinates,
    and the camera projection matrices, 
    this will triangulate to calculate 3D point, and tell wheher such a point lies in front of both cameras
    Parameters
    ----------
    P1 : TYPE
        DESCRIPTION.
    P2 : TYPE
        DESCRIPTION.
    corr1 : np array
        The point
        Column vector.
    corr2 : TYPE
        The point
        Column vector.

    Returns
    -------
    r : bool
        Whether the points are in front of both cameras.
    TDPoint : TYPE
        The coordinates of the triangulated 3D point
    """
    P1 = K @ P1;
    P2 = K @ P2;
    
    TDPoint = triangulatePoints(P1, P2, corr1, corr2);
    
    # project 
    proj1 = P1 @ TDPoint;
    proj2 = P2 @ TDPoint;
    #print('projected pts: {}, {}'.format(proj1, proj2));
    
    # if Z is positive, the point is in front of camera
    return (proj1[2] > 0 and proj2[2] > 0), TDPoint;
    
def RANSAC_EssentialMatrices(ransac_iterations, K, inputPts1, inputPts2, corresp, DEBUG = False):
    """
    Will be guessing Essential matrices, using RANSAC scheme
    
    Parameters
    ----------
    K : TYPE
        The calibration matrix.
    inputPts1 : TYPE
        The input points we work with, set 1.
    inputPts2 : TYPE
        The input points we work with, set 2.
    corresp : TYPE 
        Contains correspondences 
    DEBUG : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    foundInliers : list
        INLIERS, point wise.
    foundInliersIdx : TYPE
        indexes in source of the inliers.
    outlierIdx : TYPE
    
    """
    # the number of close data values required to assert that a model
    # fits well to data
    threshold = 5; #in pixels
    bestModelE = None;
    bestModelR = None;
    bestModelT = None;
    foundInliers = [];
    foundInlierIdx = [];
    outliers = [];
    avgReprojectionError = 0;
    
    K_inv = np.linalg.inv(K)

    for it in range(ransac_iterations):
       inliers = [];
       outliers = [];
       inlierIdx = [];
       
       # pick 5 points, and get essential matrices model from it 
       Es, pts_5 = RANSAC_pickFiveAndFindModelEssMat(inputPts1, inputPts2, corresp, K)       

       # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       # 3. Decompose every E into rotations R and translations t - four combinations
       # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
       # max 4 E matice
       Rs, ts = decomposeEssentialMatrices(Es, pts_5, K_inv);       
       
       # Select such R, t from the four combinations for which the five
       # reconstructed 3D points lie in front of both cameras
       solutionE, solutionR, solutionT, solutionTdPts, P1, P2 = RANSAC_triangulate_findSolWherePointsInFrontOfCam(
           Es, Rs, ts, pts_5, K, False);
       if(len(solutionTdPts) == 0):
           if(DEBUG):
               print('No solution found. Continue.');
           continue;
        
       if(DEBUG):
           print('Got solution');    
           print('E: {}'.format(solutionE));       
           print('R: {}'.format(solutionR));  
       
       F = K_inv.T * solutionE * K_inv# getF(K_inv, solutionE)
       # X] All other data (correspondencies) are then tested against the model
       
       # calculate the average reprojection error, for debug purposes
       avgReprojectionError_cur = 0;
       for i in range(corresp.shape[1]):
           aidx = corresp[0, i] # index first point
           bidx = corresp[1, i] # index second point
           srcPt = [getPointX(aidx, inputPts1), getPointY(aidx, inputPts1)];
           dstPt = [getPointX(bidx, inputPts2), getPointY(bidx, inputPts2)];
         
           err = calcReprojectionError(P1, P2, srcPt, dstPt, K);  
           if(DEBUG):
               print('got error {}'.format(err))
           if(abs(err) < threshold):
               # inlier?               
               #check whether they lie in front of both cameras
               inFront, TDPoint = arePtsInFrontOfCameras(P1, P2, np.array(srcPt).reshape(2, 1), np.array(dstPt).reshape(2, 1), K);
               if (inFront):
                   # really inlier                
                   inliers.append([srcPt, dstPt]);
                   inlierIdx.append([aidx, bidx]);
                   avgReprojectionError_cur = avgReprojectionError_cur + abs(err);
               else: # outlier
                   outliers.append([srcPt, dstPt]);
           else:
             outliers.append([srcPt, dstPt]);
             
       avgReprojectionError_cur = avgReprojectionError_cur / corresp.shape[1];
       if(len(inliers) > len(foundInliers)):
            if(DEBUG):
                print("[+] found better model")
            # this implies that we may have found a good model
            bestModelE = solutionE;
            bestModelR = solutionR;
            bestModelT = solutionT;
            foundInliers = inliers;
            foundInlierIdx = inlierIdx;
            avgReprojectionError = avgReprojectionError_cur;
                   
    print('most inliers: {} / {}'.format(len(foundInliers), corresp.shape[1]));
    print('average reprojection error was: {}'.format(avgReprojectionError));
    return foundInliers, foundInlierIdx, outliers, bestModelE, bestModelR, bestModelT;

def calcReprojectionError(P1, P2, srcPt, dstPt, K):
    """
    Calculates reprojection error based on 
    # TDV slide 7 prednaska 7

    Parameters
    ----------
    P1 : TYPE
        DESCRIPTION.
    P2 : TYPE
        DESCRIPTION.
    srcPt : TYPE
        DESCRIPTION.
    dstPt : TYPE
        DESCRIPTION.
    K : TYPE
        The K matrix.

    Returns
    -------
    The reprojection error.

    """
    # reprojection error 
    
    # work with P matrices, multiplied by K 
    # copying is not necessarry, but safety first
    P1 = P1.copy(); 
    P2 = P2.copy();
    
    P1 = K @ P1;
    P2 = K @ P2;
    
    #some large suma from slides -.-
    # slide 7 prednaska 7
    #uc, vc - image coordinates 
    p_1_1 = P1[0, :]; #getMatrixCol(p1, 0);
    p_1_2 = P1[1, :]; #getMatrixCol(p1, 1);
    p_1_3 = P1[2, :]; #getMatrixCol(p1, 2);
    p_2_1 = P2[0, :]; #getMatrixCol(p2, 0);
    p_2_2 = P2[1, :]; #getMatrixCol(p2, 1);
    p_2_3 = P2[2, :]; #getMatrixCol(p2, 2);
    
    #the 3D point
    #get the 3D point
    tdP = triangulatePoints(P1, P2, srcPt, dstPt);
    
    # project 
    proj1 = P1 @ tdP;
    proj2 = P2 @ tdP;
    u1 = proj1[0] / proj1[2]
    v1 = proj1[1] / proj1[2]
    u2 = proj2[0] / proj2[2]
    v2 = proj2[1] / proj2[2]
    
    #for camera 1
    val1_1 = u1 - srcPt[0]
    val2_1 = v1 - srcPt[1]
    #for camera 2
    val1_2 = u2 - dstPt[0]
    val2_2 = v2 - dstPt[1]
    
    err = (np.sqrt(((val1_1 * val1_1) + (val2_1 * val2_1))) + 
    np.sqrt(((val1_2 * val1_2) + (val2_2 * val2_2)))) / 2;
    return err;


def visualizePoints(tdPts, camC):
    """
    Allows to visualize the result

    Parameters
    ----------
    tdPts : TYPE
        The 3D points, triangulated.
    camC : The camera 2's C position
        DESCRIPTION.

    Returns
    -------
    None.

    """
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # 3. Visualize the points 
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    fig = plt.figure(figsize=(4,4))
    ax = fig.add_subplot(111, projection='3d')
    # the points 
    for o in range(len(tdPts)):
        ax.scatter(tdPts[o][0],tdPts[o][1],tdPts[o][2], marker="x", c="red") # plot the point (x, y, z) on the figure
    # plot cameras
    ax.scatter(0, 0, 0, marker="o", c="cyan")
    #ax.scatter(camC[0],camC[1],camC[2], marker="o", c="cyan")
    plt.show();