# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 09:12:35 2022

@author: Don
"""

import pptk
import numpy as np
from pyntcloud import PyntCloud
from plyfile import PlyData, PlyElement

dinosaurus = PyntCloud.from_file("./out/out.ply")
print(dinosaurus)
dinosaurus.plot()

plydata = PlyData.read('./out/out.ply')
print(plydata['vertex'])

P = [plydata['vertex']['x'], plydata['vertex']['y'], plydata['vertex']['z']]
v = pptk.viewer(P)