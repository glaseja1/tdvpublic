# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from rectify import *
from RANSAC_EssentialMatrices import *

def getF(K_inv, E):
    return K_inv.T * E * K_inv

def task4_run(K, ptsAll, corresp):
    K_inv = np.linalg.inv(K);
    
    # task 4
    
    # +++++++++++++++++++++++++++++++++++++++++
    task = [] # task variable for all data

    # For every selected image pair with indices i_a and i_b
    pairs = [[5, 6]];
    for i in range(len(pairs)):
        i_a = pairs[i][0];
        i_b = pairs[i][1];
        
        u_a = ptsAll[i_a];
        u_b = ptsAll[i_b];
        #   - load the image im_a, im_b, compute fundamental matrix F_ab
        
        im_a = plt.imread("./input/images/{}.jpg".format(str(i_a).zfill(2)));
        im_b = plt.imread("./input/images/{}.jpg".format(str(i_b).zfill(2)));
        
        print('running RANSAC')
        inliers, foundInlierIdx, outliers, bestModelE, bestModelR, bestModelT = RANSAC_EssentialMatrices(500, K, u_a, u_b, corresp[5][6], False);
        print('RANSAC ended')
        F_ab = getF(K_inv, bestModelE);
        #   - load corresponing points u_a, u_b
        #   - keep only inliers w.r.t. F_ab

        H_a, H_b, im_a_r, im_b_r = rectify.rectify( F_ab, im_a, im_b )
        print(Ha)
        print(Hb)
        print(imga_r)
        print(imgb_r)
        #   - modify corresponding points by H_a, H_b
    
        #   seeds are rows of coordinates [ x_a, x_b, y ]
        #      (corresponding point have the same y coordinate when rectified)
        #   assuming u_a_r and u_b_r euclidean correspondences after rectification:
        """
        seeds = np.vstack( ( u_a_r[0,:], u_b_r[0], ( u_a_r[1] + u_b_r[1] ) / 2 ) ).T

        task_i = np.array( [ im_a_r, im_b_r, seeds ], dtype=object )
        task += [ task_i ]
        """
    # now all stereo tasks are prepared, save to a matlab file

    task = np.vstack( task )
    scipy.io.savemat( 'stereo_in.mat', { 'task': task } )

    # here run the gcs stereo in matlab, and then load the results

    d = scipy.io.loadmat( 'stereo_out.mat' )
    D = d['D']

    # a disparity map for i-th pair is in D[i,0]
    i = 0  # 1, 2, ...
    Di = D[i,0]

    plt.imshow( Di )
    
    
    """
import scipy.io

task = [] # task variable for all data

# For every selected image pair with indices i_a and i_b

#   - load the image im_a, im_b, compute fundamental matrix F_ab
#   - load corresponing points u_a, u_b
#   - keep only inliers w.r.t. F_ab

    [H_a, H_b, im_a_r, im_b_r] = rectify.rectify( F_ab, im_a, im_b )

#   - modify corresponding points by H_a, H_b

#   seeds are rows of coordinates [ x_a, x_b, y ]
#      (corresponding point have the same y coordinate when rectified)
#   assuming u_a_r and u_b_r euclidean correspondences after rectification:
    seeds = np.vstack( ( u_a_r[0,:], u_b_r[0], ( u_a_r[1] + u_b_r[1] ) / 2 ) ).T

    task_i = np.array( [ im_a_r, im_b_r, seeds ], dtype=object )
    task += [ task_i ]

# now all stereo tasks are prepared, save to a matlab file

task = np.vstack( task )
scipy.io.savemat( 'stereo_in.mat', { 'task': task } )

# here run the gcs stereo in matlab, and then load the results

d = scipy.io.loadmat( 'stereo_out.mat' )
D = d['D']

# a disparity map for i-th pair is in D[i,0]
i = 0  # 1, 2, ...
Di = D[i,0]

plt.imshow( Di )
"""