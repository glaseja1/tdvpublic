from corresp import Corresp
import numpy as np
from RANSAC_EssentialMatrices import *
from tdvTask2Utils import *
from p3p.p3p import *
import math

"""
print(np.array([2])**2)
# dummy matice 3xn (zde 3x3)
matrix_3n = np.array([[1, 2, 3],[4, 5, 6], [7,8,9]])
matrix_3n = sum(matrix_3n**2);
# [14 17 16]
print(matrix_3n)
# odmocnina z toho nejde
matrix_3n = math.sqrt(matrix_3n)
print(matrix_3n)

sqrt(sum(   (K @ P_i @ pointCloud[X, :] - pts[i][u, :])^2)      )
"""

def calcErrorThresholding(K, Pi, tdPts4xn_all, ptsMat3xn):
    """
    Calculates error for each point

    Parameters
    ----------
    K : TYPE
        DESCRIPTION.
    Pi : TYPE
        DESCRIPTION.
    tdPts4xn_all : TYPE
        DESCRIPTION.
    ptsMat3xn : TYPE
        DESCRIPTION.

    Returns
    -------
    list
        list of error values for all points.

    """
    #inliers = sqrt(sum((K @ P_i @ tdPts - pts[i][u, :])^2));
    n = tdPts4xn_all.shape[1];
    
    # (3x3 matrix, 3x4 matrix, 4xn matrix) = 3xn matrix
    tmp = K @ Pi @ tdPts4xn_all;
    # normalize (2xn matrix)
    #print(tmp)
    for cc in range(n):
        tmp[0][cc] = tmp[0][cc] / tmp[2][cc];
        tmp[1][cc] = tmp[1][cc] / tmp[2][cc];
       
    # delete the third row
    tmp = np.delete(tmp, (2), axis=0);
    #print(tmp)
    
    # 2xn matrix - 2xn matrix = 2xn matrix
    #print('temp res: {}'.format(tmp - ptsMat))
    tmp = sum((tmp - ptsMat3xn)**2);           
    #print(tmp)
    
    for cc in range(n):
        tmp[cc] = math.sqrt(tmp[cc]);
        
    return tmp;
        
def ransacRT(ransac_iterations, tdPts, 
             ptsXes,
             ptsYes, K, DEBUG = False):
    """
    Estimates R, T using ransac

    Parameters
    ----------
    ransac_iterations : TYPE
        DESCRIPTION.
    tdPts : np array, 3xn matrix
        DESCRIPTION.
    ptsXes : array, length of n
        DESCRIPTION.
    ptsYes : array, length of n
        DESCRIPTION.
    DEBUG : TYPE, optional
        DESCRIPTION. The default is False.

    Raises
    ------
    
        DESCRIPTION.

    Returns
    -------
    bestModelR : TYPE
        DESCRIPTION.
    bestModelT : TYPE
        DESCRIPTION.
    TYPE
        DESCRIPTION.

    """
    if(len(ptsXes) != len(ptsYes)):
        raise "expected equal length of arrays for points";
    if(tdPts.shape[1] != len(ptsYes)):
        raise "3D point matrix 3xn, n is not equal to 2D point array length";
    if(tdPts.shape[0] != 3):
        raise "3D point matrix is expected to be a 3xn matrix";
        
    threshold = 5; #in pixels
    bestModelR = None;
    bestModelT = None;
    foundInliers = [];
    foundInlierIdx = [];
    n = tdPts.shape[1];
    
    if( n < 3 ):
        raise "Impossible to run the 3 point algorithm. Not enough 3D points (need 3 or more)";
        
    for it in range(ransac_iterations):
       R = [];
       t = [];
       inliers = [];
        
       # 1. Chose a random 3 of 3D points 
       indexes = random.sample(range(0, n), 3);
       
       # ----------------------------------
       # for p3p grunnert
       # p3p_grunert() žere 3D point a 2D point projection 
       # ----------------------------------
       #create input matrix 4x3
       tdmat = np.array(tdPts[:, indexes[0]]).reshape(3, 1);
       # add columns
       tdmat = np.hstack( ( tdmat, np.array(tdPts[:, indexes[1]]).reshape(3, 1) ) )
       tdmat = np.hstack( ( tdmat, np.array(tdPts[:, indexes[2]]).reshape(3, 1) ) )
       # add ones as a line
       tdmat = np.vstack( ( tdmat, np.ones((3), dtype=int) ) )
      
       #print('tdmat: {}'.format(tdmat));
       # the three point projections (homogeneous, 3x3 matrix, row vectors).       
       #create u matrix 3x3
       u = np.array([ptsXes[indexes[0]], ptsYes[indexes[0]], 1]).reshape(3, 1);
       # add columns
       for i in range(1, 3):
           pt = np.array([ptsXes[indexes[i]], ptsYes[indexes[i]], 1]).reshape(3, 1);
           u = np.hstack( ( u, pt ) )
       #print('u: {}'.format(u));
       
       Xc = p3p_grunert(tdmat, np.linalg.inv(K) @ u);
       # najde odpovídající 3D body X_c v souřadné soustavě i-té kamery 
       # (až čtyři řešení, je třeba přes ně iterovat)
       #print('Xc: {}'.format(Xc));
        
       # ----------------------------------
       # ----------------------------------
       # ----------------------------------
       
       #XX2Rt_simple() najde rotaci a translaci ze světové souřadné soustavy do souřadného systému nové kamery
       #argument: 4xn matrix
       
       tdPts4xn_all = tdPts;
       # add ones as a line
       tdPts4xn_all = np.vstack( ( tdPts4xn_all, np.ones((n), dtype=int) ) )
       
       # iterate solutions 
       for i in range(len(Xc)):
           soli = Xc[i];
           #print('Xc solution: {}'.format(Xc[i]));
           
           # estimate R, T
           R, t = XX2Rt_simple(tdmat, soli);
           #print('R: {}', R)
           #print('t: {}', t)
           
           Pi = constructCamera(R, t);
           
           """
           print('K: {}'.format(K))
           print('P_i: {}'.format(Pi))
           print('tdPts: {}'.format(tdPts))
           print('tdPts4xn_all: {}'.format(tdPts4xn_all))           
           print('pts xes: {}'.format(ptsXes))
           print('pts yes: {}'.format(ptsYes))
           """
           #ptsMat = np.array([ptsXes, ptsYes, 1]).reshape(3, 1);
           
           #3xn matrix
           ptsMat = np.array(ptsXes);
           ptsMat = np.vstack( ( ptsMat, np.array(ptsYes) ) )
           #print('ptsMat: {}'.format(ptsMat))
           err = calcErrorThresholding(K, Pi, tdPts4xn_all, ptsMat);
           
           #pick inliers that satisfy the error threshold
           inlierIdx = np.where(err < threshold);
           #print(tmp[inlierIdx])
           inliers = err[inlierIdx];
           #print(len(inliers))
           #ptsYes
                      
           if(bestModelR is None or len(inliers) > len(foundInliers)):
               #print('[+] got inliers: {}'.format(len(inliers)));
               foundInliers = inliers;
               foundInlierIdx = inlierIdx;
               bestModelR = R;
               bestModelT = t;
           
      
    if(len(foundInliers) == 0):
        raise "Found no inliers. Stop.";
    print('most inliers: {}'.format(len(foundInliers)));        
    return bestModelR, bestModelT, foundInliers, foundInlierIdx;

def refineE():
    pass 

def cameraGluing(pts, correspAll, camCnt, K):
    """
    

    Parameters
    ----------
    pts : TYPE
        All the input points.

    Returns
    -------
    pointCloud3n: np.array
        3xn matrix of the point cloud.
    camGlueOrder
    cameraRs
    cameraTs
    """
    camGlueOrder = [];
    #R matrices of cameras, for all cameras (1 up to n) by index 
    cameraRs = [];    
    #R matrices of cameras, for all cameras (1 up to n) by index 
    cameraTs = [];    
    # add dummy sockets
    for cam1 in range(0, camCnt): 
        cameraRs.append([]);
        cameraTs.append([]);
    
    # the point cloud
    pointCloud = [];
    # the point cloud as 3xn matrix
    pointCloud3n = [];
    
    # initialize with number of cameras
    c = Corresp( camCnt );
    # import all tentative image to image correspondences 
    for cam1 in range(1, camCnt): 
        for cam2 in range(cam1 + 1, camCnt): 
                        
            inpt = correspAll[cam1][cam2].T;
            #print(inpt)
            """
            [[    0  7433]
             [    1  6337]
             [    3     7]
             ...
             [28281     3]
             [28285 28378]
             [28294 25178]]
            """
            
            c.add_pair( cam1, cam2, inpt);
    
    # select two cameras
    #  Initialisation of Camera Cluster and Point Cloud
    i1 = 1; 
    i2 = 2; # need not be ordered, the function takes care itself
    [m1, m2] = c.get_m( i1, i2 ); # get image-to-image correspondences

    #+++++++++++++++++++++++++++++++++++++
    # run RANSAC to get inliers
    #+++++++++++++++++++++++++++++++++++++

    #Estimate epipolar geometry using correspondences m12. 
    #Obtain set of inliers, reconstruct cameras and 3D scene points.
    corresp = np.array([m1, m2]);
    #print(corresp)
    inputPts1 = pts[i1];
    inputPts2 = pts[i2];
    # estimate E, R, T using RANSAC
    inliers, foundInlierIdx, outliers, bestModelE, bestModelR, bestModelT = RANSAC_EssentialMatrices(120, K, inputPts1, inputPts2, corresp, False);
    #F = getF(K_inv, bestModelE);

    """
    print('inliers:')
    print(inliers)
    print('foundInlierIdx:')    
    print(foundInlierIdx)
    """
    #+++++++++++++++++++++++++++++++++++++

    """
    print('corresp:')
    print(corresp)
    """
    # inl is array of indices to m12 – obtained inliers
    # so for example [2, 3, 8, 15]
    inl = gluing_findIndexesInCorresp(foundInlierIdx, corresp);
    """
    print("indices:")
    print(inl)
    """
    if(len(inl) == 0):
        raise Exception("camera gluing: no inliers found");
    
    # the first camera is canonical
    cameraRs[1] = np.eye(3);
    cameraRs[2] = bestModelR;
    cameraTs[1] = [0, 0, 0];
    cameraTs[2] = [bestModelT[0], bestModelT[1], bestModelT[2]];
    # reconstruct cameras and initial point cloud
    P1 = constructCameraCanonical();
    P2 = constructCamera(bestModelR, bestModelT);
    for i in range(len(inliers)):
        corr1 = inliers[i][0];
        corr2 = inliers[i][1];
        
        # check for the points in front of cameras
        inFront, TDPoint = arePtsInFrontOfCameras(P1, P2, corr1, corr2, K);
        if(inFront):
            #TDPoint = triangulatePoints(K @ P1, K @ P2, corr1, corr1);
            pointCloud.append(TDPoint);
            
            pt = np.array([TDPoint[0], TDPoint[1], TDPoint[2]]).reshape(3, 1);              
            if(len(pointCloud3n) == 0):
                pointCloud3n = pt;
            else: 
                # add column
                pointCloud3n = np.hstack( ( pointCloud3n, pt ) );
            
    #print(pointCloud3n)
    #print('inl:')
    #print(inl);
    #print(inl.shape);
    # inl = indexy validnich bodu correspondeces
    c.start( i1, i2, inl );
    
    #print('point cloud:')
    #print(pointCloud);
    # +++++++++++++++++++++++++++++++++++
    
    #P matrices of cameras, for all cameras (1 - n) by index 
    cameraPs = [];
    #dummy
    cameraPs.append([]);
    cameraPs.append(P1);
    cameraPs.append(P2);
    # add dummy sockets
    for cam1 in range(1, camCnt - 2): 
        cameraPs.append([]);
    
    while(True):
        
        # list of cameras with tentative scene-to-image correspondences
        # counts of tentative correspondences in each ‘green’ camera
        ig, Xucount = c.get_green_cameras();
        # get the camera with the most data 
        #print(Xucount)
        if(len(Xucount) == 0 or np.argmax(Xucount) < 0 or np.argmax(Xucount) >= len(ig)):
            print('no more cameras. Break.');
            break;
            
        # select the most promising camera
        i = ig[np.argmax(Xucount)];
        camGlueOrder.append(i);
        print('selected camera: {}'.format(i));
        #TODO: put here the camera proj matrix related to i
        #print(Xucount)
        #TODO: tohle bere jako argument ty 3D body jen z te jedne iterace
        
        #ilist = c.get_selected_cameras(); # list of all cameras in the cluster
        #ic = 1; # a camera in the cluster (must be iterated through ilist).
        # scene-to-image (3D-2D) tentative korespondence pro i-tou kameru
        X, u, Xu_verified = c.get_Xu( i );
        #print(X)
        #print(u)
        #print(len(X))
        #print(len(u))
        # tedy pointCloud[X, :] coresponduje s pts[i][u, :]
        
        #print('pointCloud3n[:, X]')
        #print(pointCloud3n[:, X])
        #print(pointCloud3n[:, X].shape)
        #print('pts')
        #print(pts)        
        #print(i)
        #print(u)
        
        ptsXes = pts[i][0][u];
        ptsYes = pts[i][1][u];
        #print(ptsXes)
        #print(ptsYes)
        
        R, T, inliers, inlierIdx = ransacRT(100, pointCloud3n[:, X], ptsXes, ptsYes, K, True);
        #print(inlierIdx[0])
        
        #+++++++++++++++++++
        # camera refinement
        #+++++++++++++++++++
        #x = fmin(refineE, np.block([0, 0, 0, t[0], t[1], t[2]]), )
        
        #minimalizovat error kde jsou 3 parametry rot a 3 translate 
        #ransac vrati R0 
        
        #+++++++++++++++++++
        P3 = constructCamera(R, T);    
        cameraPs[i] = P3;
        cameraRs[i] = R;
        cameraTs[i] = [T[0][0], T[1][0], T[2][0]];
        #i ... camera to be attached, based on XCount    
        xinl = inlierIdx[0]; #[1, 2]; # obtained inliers – indices to i-th Xu
        c.join_camera( i, xinl );


        #get the row vector of neighbouring (to the attached) cameras
        ilist = c.get_cneighbours(i);
        #this gives cameras that are related to the attached camera, based on the correspondences
        
        print('neighbour cameras: {}'.format(ilist))
                        
        #iterate the neighbour cams 
        for cit in range(0, len(ilist)):
            ic = ilist[cit];
        
            cam1 = min(i, ic)
            cam2 = max(i, ic)
            
            """print('for cams: {}-{}:'.format(cam1, cam2))
            print('cam{} P: {}'.format(i, cameraPs[i]));
            print('cam{} P: {}'.format(ic, cameraPs[ic]));
            """
            #print('correspondences all: {}'.format(correspAll[cam1][cam2].T))
                
            [ mi, mic ] = c.get_m( i, ic ); # get remaining image-to-image correspondences
            
            #print(mi)
            #print(mic)            
                       
            # Reconstruct new scene points using the cameras i and ic
            
            # Obtained inliers–indices to m
            inl = [];
            for qqqqq in range(len(mi)):
                idxA = mi[qqqqq];
                idxB = mic[qqqqq];
                                
                #print('correspondence idx: {}, {}'.format(idxA, idxB));
                
                # get the points using camera indexes and correspondence indexes
                #print( pts[i] )
                #print( pts[ic] )                
                
                srcPt = np.array([getPointX(idxA, pts[i]), getPointY(idxA, pts[i])]).reshape(2, 1);    
                dstPt = np.array([getPointX(idxB, pts[ic]), getPointY(idxB, pts[ic])]).reshape(2, 1);                
                
                #print(srcPt)
                #print(dstPt)
                #corr1 = inliers[idxA][0];
                #corr2 = inliers[idxB][1];
                
                # check for the points in front of cameras
                inFront, TDPoint = arePtsInFrontOfCameras(cameraPs[i], cameraPs[ic], srcPt, dstPt, K);
                
                POINT1 = K @ cameraPs[i] @ TDPoint
                POINT1 /= POINT1[2]
                
                POINT2 = K @ cameraPs[ic] @ TDPoint
                POINT2 /= POINT2[2]
                # also apply the reprojection error
                err = np.sqrt(np.sum((POINT1[:2] - srcPt.T) ** 2)) + np.sqrt(np.sum((POINT2[:2] - dstPt.T) ** 2))
                #print(err)                
                if(inFront and err < 6):
                    pointCloud.append(TDPoint);
                    inl.append(qqqqq);
                    
                    pt = np.array([TDPoint[0], TDPoint[1], TDPoint[2]]).reshape(3, 1);              
                    if(len(pointCloud3n) == 0):
                        pointCloud3n = pt;
                    else: 
                        # add column
                        pointCloud3n = np.hstack( ( pointCloud3n, pt ) );
                        
            #print(inl)
            c.new_x( i, ic, inl, None );
            
            #assert(11==0)
                                    
                        
             
        # Verification of Tentative Scene-to-Image Correspondences in the Cluster
        
        
        ilist = c.get_selected_cameras(); # list of all cameras in the cluster
        #print(ilist)
        for zzo in range(len(ilist)):
            ic = ilist[zzo]; # a camera in the cluster (must be iterated through ilist).
            X, u, Xu_verified = c.get_Xu( ic );
            #print('Xu_verified: {}'.format(Xu_verified));
            #Xu_tentative = find( ~Xu_verified );
            
            #Verify (by reprojection error) scene-to-image correspondences in Xu_tentative. A subset of good
            #points is obtained.
            #err = calcErrorThresholding(K, cameraPs[ic], tdPts4xn_all, ptsMat3xn):
            
            corr_ok = []; # The subset of good points—there is no one here.
            for pppps in range(len(Xu_verified)):
                if(not Xu_verified[pppps]):
                    corr_ok.append(pppps);
            
            # TODO: there are no unverified ones
            #print(corr_ok)
            c.verify_x( ic, corr_ok );        
        
        #finalize
        c.finalize_camera();

    return pointCloud3n, camGlueOrder, cameraRs, cameraTs;