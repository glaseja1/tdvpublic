"""
implementation based on TDV slides
"""
import numpy as np
from utils import *

def triangulatePoints(p1, p2, projPt1, projPt2, DEBUG = False):
    """
    Given cameras P1, P2 and a correspondence x ↔ y compute a 3D point X
projecting to x and y

    Parameters
    ----------
    p1 : the projection matrix of the first camera
        DESCRIPTION.
    p2 : the projection matrix of the second camera
        DESCRIPTION.
    projPt1 : list 
        The point, seen by first camera
    projPt2 : list
        The point (correspondence), seen by second camera

    Returns
    -------
    The 3D point.

    """
    
    if(DEBUG):
        print()
        print()
        print()
        print()
        print()
        print('point 1 = {}'.format(projPt1))
        print('point 2 = {}'.format(projPt2))
        print('P1 = {}'.format(p1))
        print('P2 = {}'.format(p2))
    # p_a_b  means that it is P_a matrix, bth column. a in index above the p, b is index below the p
    u1 = projPt1[0];
    v1 = projPt1[1];
    u2 = projPt2[0];
    v2 = projPt2[1];
    p_1_1 = p1[0, :]; #getMatrixCol(p1, 0);
    p_1_2 = p1[1, :]; #getMatrixCol(p1, 1);
    p_1_3 = p1[2, :]; #getMatrixCol(p1, 2);
    p_2_1 = p2[0, :]; #getMatrixCol(p2, 0);
    p_2_2 = p2[1, :]; #getMatrixCol(p2, 1);
    p_2_3 = p2[2, :]; #getMatrixCol(p2, 2);
    
    if(DEBUG):
        print(projPt1 )
        print(u1 )
        print(p_1_3)
        print(u1 * p_1_3)
        
    D = np.array([
        u1 * p_1_3 - p_1_1,
        v1 * p_1_3 - p_1_2,
        u2 * p_2_3 - p_2_1,
        v2 * p_2_3 - p_2_2
        ]);
    
    # D is 4x4
    
    # add column of zeros
    #D = addColToMatrix(D, np.zeros( ( 4, 1 ) ))
    if(DEBUG):
        print('')
        print('D = {}'.format(D))
    
    Q = D.T @ D;
    if(DEBUG):
        print('')
        print('Q = {}'.format(Q))
        print('')
    U, D, V_T = np.linalg.svd(Q)
    
    X = getMatrixCol(U, 3); #or U[:, -1]
    #X = np.linalg.solve(D, np.zeros((4, 1)))
       
    """
    4 sloupec U je X
    """
    #U, D, V_T = np.linalg.svd(D)
    #X = getMatrixCol(V_T, 2); 
    if(DEBUG):  
        print('U = {}'.format(U))  
        #print('D = {}'.format(D)) 
        print('')
        print(X)   
    result = [X.item(0), X.item(1), X.item(2), X.item(3)];
    result[0] = result[0] / result[3]
    result[1] = result[1] / result[3]
    result[2] = result[2] / result[3]
    result[3] = result[3] / result[3]
    return result;