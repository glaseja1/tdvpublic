import numpy as np

# helpers
def getPointX(index, pts):
    return pts[0][index]
def getPointY(index, pts):
    return pts[1][index]

def getCorrespondencyTo(pointIndex, pointsU2, correspDict):
    dstIndex = correspDict[pointIndex];
    x = getPointX(dstIndex, pointsU2);
    y = getPointY(dstIndex, pointsU2);
    return [x, y];

def constructCameraCanonical():
    P1 = np.eye(3);
    # add column
    P1 = np.hstack( ( P1, np.zeros( ( 3, 1 ) ) ) )
    return P1;


def constructCamera(R, t):
    P2 = R;
    # add column of the T
    P2 = np.hstack( ( P2, np.array(t).reshape(3, 1) ) );
    return P2;

def gluing_findIndexesInCorresp(inlierIdx, corresp, DEBUG = False):
    """
    Gives the indexes of inliers in corresp
    
    Complexity: O(n*m)

    Parameters
    ----------
    inlierIdx : list if x, y, indexes
        for example [[0, 7433], [28294, 25178], [28294, 0]]
    corresp : np array
        DESCRIPTION.
    DEBUG : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    if(DEBUG):
        print('gluing_findIndexesInCorresp')
        print(inlierIdx)
    # get indexes of inliers in corresp    
    idx = [];
    for i in range(len(inlierIdx)):
        d = inlierIdx[i];
        if(DEBUG):
            print('iterate')
            print(d)
        for x in range(len(corresp[0])):
            if(corresp[0][x] == d[0] and corresp[1][x] == d[1]):
                idx.append(x);
                break;
            
    return idx;