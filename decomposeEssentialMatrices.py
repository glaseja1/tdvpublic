import numpy as np
from utils import *

def decomposeEssentialMatrices(Es, pts_5, K_inv, DEBUG = False):
    """
    Decomposes essential matrices to R, T matrices

    Parameters
    ----------
    Es : TYPE
        DESCRIPTION.
    pts_5 : TYPE
        DESCRIPTION.
    K_inv : ndarray
        the inverted K matrix.
    DEBUG : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    Rs : TYPE
        R matrices for each essential matrix
    ts : TYPE
        t vectors for each essential matrix

    """
    # R matrices for each essential matrix
    Rs = [];
    # t vectors for each essential matrix
    ts = [];

    for idxE in range(len(Es)):
        Rs.append([]);
        ts.append([]);
        
        E = Es[idxE];
        if(DEBUG):
            print('E: {}'.format(E))
        # lecture 7
        # slide 5-Point Algorithm for Relative Camera Orientation

        #vte = np.matmul(v.reshape(1, 3), E)
        # 3x1 and 1x3 = 3x3
        #vivevi = vte.reshape(3, 1) @ v_prime.reshape(1, 3);
        #print(vivevi)

        U, D, V_T = np.linalg.svd(E);
        #A = mxn = 3x3
        #U = mxm = 3x3
        #D = mxn = 3x3
        #V_T = nxn = 3x3

        #TODO: extract the R and t

        #### this is from internet:
        # t shall be the last column in U
        t = U[:, -1]
        # add t there 4x, as it is the same for each R?
        ts[idxE].append(t);
        ts[idxE].append(-t);
        ts[idxE].append(t);
        ts[idxE].append(-t);
        if(DEBUG):
            print('t: {}'.format(t))
        #R = U * [?] * V^T
        Y = np.array([
            [0, -1, 0],
             [1, 0, 0],
             [0, 0, 1]
             ]);
        R1 = (U @ Y) @ V_T;
        R2 = (U @ Y) @ V_T;
        R3 = (U @ Y.T) @ V_T;
        R4 = (U @ Y.T) @ V_T;
        
        Rs[idxE].append(R1);
        Rs[idxE].append(R2);
        Rs[idxE].append(R3);
        Rs[idxE].append(R4);
        if(DEBUG):
            print('R1: {}'.format(R1))
            print('R2: {}'.format(R2))
            print('R3: {}'.format(R3))
            print('R4: {}'.format(R4))
            
        """
        # run 5 point algorithm
        for i in range(len(pts_5)):
            m = toHomo(pts_5[i][0]);
            m_prime = toHomo(pts_5[i][1]);
            v = K_inv @ m;
            v_prime = K_inv @ m_prime;
            #print(v)
            #print(v_prime)
            # get the viT * E vi
            # 1x3 and 3x3 = 1x3
            vte = np.matmul(v.reshape(1, 3), E)
            # 3x1 and 1x3 = 3x3
            vivevi = vte.reshape(3, 1) @ v_prime.reshape(1, 3);
            #print(vivevi)

            U, D, V_T = np.linalg.svd(E);
            #A = mxn = 3x3
            #U = mxm = 3x3
            #D = mxn = 3x3
            #V_T = nxn = 3x3

            #TODO: extract the R and t

            #### this is from internet:
            # t shall be the last column in U
            t = U[:, -1]
            # add t there 4x, as it is the same for each R?
            ts[idxE].append(t);
            ts[idxE].append(-t);
            ts[idxE].append(t);
            ts[idxE].append(-t);
            if(DEBUG):
                print('t: {}'.format(t))
            #R = U * [?] * V^T
            Y = np.array([
                [0, -1, 0],
                 [1, 0, 0],
                 [0, 0, 1]
                 ]);
            R1 = (U @ Y) @ V_T;
            R2 = (U @ Y) @ V_T;
            R3 = (U @ Y.T) @ V_T;
            R4 = (U @ Y.T) @ V_T;
            
            Rs[idxE].append(R1);
            Rs[idxE].append(R2);
            Rs[idxE].append(R3);
            Rs[idxE].append(R4);
            if(DEBUG):
                print('R1: {}'.format(R1))
                print('R2: {}'.format(R2))
                print('R3: {}'.format(R3))
                print('R4: {}'.format(R4))
        """
    return Rs, ts;
