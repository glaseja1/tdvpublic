import numpy as np
import p5

def calcEssentialMatrices(u1p, u2p, K):
    """
    
    Parameters
    ----------
    u1p : TYPE
        DESCRIPTION.
    u2p : TYPE
        DESCRIPTION.
    K : TYPE
        The calibration matrix.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    
    
    """
    dummy
    E = [[1, 1, 1],
         [2, 2, 2],
          [3, 3, 3]]
    return [E]
    """
    
    # Essential matrix works with K^-1 points 
    # convert to homo
    u1p = np.linalg.inv(K) @ np.vstack( ( u1p, np.ones( ( 1, 5 ) ) ) )
    u2p = np.linalg.inv(K) @np.vstack( ( u2p, np.ones( ( 1, 5 ) ) ) )
    #print('u1p: {}'.format(u1p))
    #print('u2p: {}'.format(u2p))
    Es = p5.p5gb( u1p, u2p );
    return Es;
