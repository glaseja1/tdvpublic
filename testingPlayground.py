import numpy as np

def run():
    a = np.array([1, 2, 3])
    b = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    c = np.array([[1], [2], [3]])
    ab = np.matmul(a, b)
    print (ab)
    print (c)
    print(ab.reshape(3, 1) @ c.reshape(1, 3))