import testingPlayground
import numpy as np
import random
from calcEssentialMatrices import *
from decomposeEssentialMatrices import *
from triangulate import *
import utils
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from RANSAC_EssentialMatrices import *
from tdvTask2Utils import *
import p3p
from cameraGluing import *
import pickle
import ge
from pyntcloud import PyntCloud
from task4 import *

LOAD_RESULTS_FROM_FILE = True;

# make sure my utils are ok
utils.runTests();

def runAsserts():
    """
    Runs asserts to make sure my data loading is OK.
    These asserts were made for point data sets 5 and 6

    Returns
    -------
    None.

    """
    # camera gluing utils 
    inlierIdx = [[0, 7433], [28294, 25178], [28294, 0]];    
    corresp = np.array([[0, 0,  0,1,3,81,28285,28294],
     [7433,1, 7434, 6337,7,3,28378,25178]]);
    r = gluing_findIndexesInCorresp(inlierIdx, corresp);
    assert(len(r) == 2);
    assert(r[0] == 0);
    assert(r[1] == 7);
    
    """assert getPointX(0, pts6) == 6.3;
    assert getPointY(0, pts6) == 1749.0;
    assert existsCorrespondencyFor(4) == True;
    assert existsCorrespondencyFor(7) == True;
    assert getPointX(4, pts5) == 10.9;
    assert getPointY(4, pts5) == 571.7;
    xxx = getCorrespondencyTo(4, pts6);
    assert xxx[0] == 11.3;
    assert xxx[1] == 583.4;

    #assert the K matrix data loading
    assert K[0][0] == 2080;
    assert K[0][1] == 0;
    assert K[0][2] == 1421;
    assert K[1][0] == 0;
    assert K[1][1] == 2080;
    assert K[1][2] == 957;
    assert K[2][0] == 0;
    assert K[2][1] == 0;
    assert K[2][2] == 1;

    M = getMatrixCol([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 2);
    assert M[0] == 3
    assert M[1] == 6
    assert M[2] == 9
    M = getMatrixCol([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 1);
    assert M[0] == 2
    assert M[1] == 5
    assert M[2] == 8
    M = getMatrixCol(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]), 1);
    assert M[0] == 2
    assert M[1] == 5
    assert M[2] == 8"""

def loadCorrespondencies(path):
    """
    Loads correspondency data from a file

    Parameters
    ----------
    path : string
        Path to the file, relative.

    Returns
    -------
    corresp : dict
       DESCRIPTION
    """
    #load data
    corresp = np.loadtxt( path ).astype(int).T
    #print(corresp)
    return corresp;

def loadPoints(path):

    #load points
    pts=np.loadtxt( path ).T
    return [pts[0, :], pts[1, :]];

def loadK(path):
    return np.loadtxt( path );

def existsCorrespondencyFor(pointIndex):
    if( pointIndex in correspDict):
        return True;
    return False;

def getF(K_inv, E):
    return K_inv.T * E * K_inv

runAsserts();


NEEDLES = True;
img = plt.imread("./input/images/05.jpg");

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# load 
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
K = loadK('input/K.txt');
K_inv = np.linalg.inv(K);
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# load points
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
pts = ['x'];
pts.append(loadPoints( 'input/corresp/u_01.txt' ));
pts.append(loadPoints( 'input/corresp/u_02.txt' ));
pts.append(loadPoints( 'input/corresp/u_03.txt' ));
pts.append(loadPoints( 'input/corresp/u_04.txt' ));
pts.append(loadPoints( 'input/corresp/u_05.txt' ));
pts.append(loadPoints( 'input/corresp/u_06.txt' ));
pts.append(loadPoints( 'input/corresp/u_07.txt' ));
pts.append(loadPoints( 'input/corresp/u_08.txt' ));
pts.append(loadPoints( 'input/corresp/u_09.txt' ));
pts.append(loadPoints( 'input/corresp/u_10.txt' ));
pts.append(loadPoints( 'input/corresp/u_11.txt' ));
pts.append(loadPoints( 'input/corresp/u_12.txt' ));

#str(cFrom).zfill(2)
            
#the set the program will work with (the initial RANSAC)
inputPts1 = pts[5];
inputPts2 = pts[6];

camCnt = 12;
# init
# create list of all corespondences, later on used by the program
corresp = [];
for cFrom in range(0, camCnt + 1):
    corresp.append([]);
    for cTo in range(0, camCnt + 1):
        corresp[cFrom].append([]);

for cFrom in range(1, camCnt):
    for cTo in range(cFrom + 1, camCnt):
        if(cFrom == cTo):
            continue;
        corresp[cFrom][cTo] = loadCorrespondencies('input/corresp/m_{}_{}.txt'.format(str(cFrom).zfill(2), str(cTo).zfill(2)));


# +++++++++++++++++++++++++++++++
# asserts
# these asserts are for correspondencies data "05_06"
#runAsserts();
# +++++++++++++++++++++++++++++++
#print(K);

#print(correspoPoll)

# +++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++

if(not LOAD_RESULTS_FROM_FILE):
    # estimate E, R, T using RANSAC
    inliers, foundInlierIdx, outliers, bestModelE, bestModelR, bestModelT = RANSAC_EssentialMatrices(100, K, inputPts1, inputPts2, corresp[5][6], False);
    F = getF(K_inv, bestModelE);
    
    inliersLoaded = [];
    with open("saves/inliers", "rb") as fp:   
        inliersLoaded = pickle.load(fp)
        
    if(len(inliers) > len(inliersLoaded)):
        print("Got more inliers ({}) than in saves ({}). Saving results to a file".format(len(inliers), len(inliersLoaded)))
            
        # save the inliers to a file 
        with open("saves/inliers", "wb") as fp:
            pickle.dump(inliers, fp)
        with open("saves/foundInlierIdx", "wb") as fp:
            pickle.dump(foundInlierIdx, fp)
        with open("saves/outliers", "wb") as fp:
            pickle.dump(outliers, fp)
        with open("saves/bestModelE", "wb") as fp:
            pickle.dump(bestModelE, fp)
        with open("saves/bestModelR", "wb") as fp:
            pickle.dump(bestModelR, fp)
        with open("saves/bestModelT", "wb") as fp:
            pickle.dump(bestModelT, fp)
        print("saving done")
    else:
        print("Not saving result. In saves got more inliers ({}) than from this run ({})".format(len(inliersLoaded), len(inliers)))
        
else:
    #loading
    print("Loading results from a file")
    with open("saves/inliers", "rb") as fp:   
        inliers = pickle.load(fp)
    with open("saves/foundInlierIdx", "rb") as fp:
        foundInlierIdx = pickle.load(fp)
    with open("saves/outliers", "rb") as fp:
        outliers = pickle.load(fp)
    with open("saves/bestModelE", "rb") as fp:
        bestModelE = pickle.load(fp)
    with open("saves/bestModelR", "rb") as fp:
        bestModelR = pickle.load(fp)
    with open("saves/bestModelT", "rb") as fp:
        bestModelT = pickle.load(fp)
        
    print("Loaded {} inliers".format(len(inliers)))
        
def displayLiers(ax1, liers, color, needles):
    xes = [];
    yes = [];
    cor_xes = [];
    cor_yes = [];
    
    back_xes = [];
    back_yes = [];
    for i in range(len(liers)):
        xes.append(liers[i][0][0])
        yes.append(liers[i][0][1])
        
        cor_xes.append(liers[i][1][0])
        cor_yes.append(liers[i][1][1])
        
    #ax1.scatter(xes, yes);
    if(needles):
        ax1.plot([xes, cor_xes], [yes, cor_yes], color=color);

"""
fig, ax1 = plt.subplots()
ax1.imshow(img)
displayLiers(ax1, outliers, 'black', NEEDLES);
displayLiers(ax1, inliers, 'red', NEEDLES);
"""  

# from the RT found, create P1, P2 again and again recreate 3D points for all 
# inlier correspondencies.
# Now also again check if such point is in front of both cameras, so some 
# 3D points will not make it to the 3D cloud 


# Appending of a Single Camera
# Select an image (Ij) that has not a camera estimated yet.
# Find image points in Ij, that correspond to some allready reconstructed 3D 
# points in the cloud (the correspondences need not be 1:1)
# Estimate the global pose and orientation of the camera Pj using the P3P algorithm in RANSAC scheme.



# moravj34@fel.cvut.cz
# system + python verze   (3.7.9)

# ++++++++++++++++++++++++
# feedback
# ++++++++++++++++++++++++

# rika ktere body jsou zrek. pro 3 obrazek
# refinement ma zlepsit rotaci a translaci 
# fmin() ze scipy  udela refinement
# c getneighbours najde sousedy

# ++++++++++++++++++++++++

pointCloud3n, camGlueOrder, cameraRs, cameraTs = cameraGluing(pts, corresp, camCnt, K);

# save the point cloud to a file 
g = ge.GePly( './out/out.ply' );
g.points( pointCloud3n ); # Xall contains euclidean points (3xn matrix), ColorAll RGB colors (3xn or 3x1, optional)
# ge has no function for saving cameras
"""
for i in range(len(cameraTs)):
    Pxx = cameraRs[i] @ cameraTs[i];
    g.cams( Pxx, 'plot', 1 );
"""
g.close();

#ge = ge_vrml( 'out.wrl' );
#ge = ge_cams( ge, Rt, 'plot', 1 );  #% P is a cell matrix containing cameras without K,
#                                % { ..., [R t], ... }
#ge = ge_points( ge, Xcloud );       #% Xcloud contains euclidean points (3xn matrix)
#ge = ge_close( ge );

print("[+] Point cloud exported to ./out/out.ply");
print("[+] The camera gluing order was {}".format(camGlueOrder));
print("[+] camera Rs: {}".format(cameraRs));

print("[+] Finished");

#output the camera order ans Ts to a file
with open('./out/pointData.js', 'w') as f:
    f.write('var POINT_DATA = [');
    for tit in range(len(cameraTs)):
        # skip the index, as it is indexed from 1
        if(tit == 0):
            continue;
            
        f.write('[{}, {}, {}]'.format(cameraTs[tit][0], cameraTs[tit][1], cameraTs[tit][2]))
        if(tit != len(cameraTs) - 1):
            f.write(',');
            
    f.write('];');
    f.write("\n");
    
    strin = '';
    for i in range(len(camGlueOrder)):
        if(i != 0):
            strin = strin + ',';
        strin = strin + '{}'.format(camGlueOrder[i]);
    f.write('var CAM_ORDER = [{}];'.format(strin))
#task4_run(K, pts, corresp);